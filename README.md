# hello_world
This repository containers a hello world workflow.

INSTALLATION
-------------
Relative hello world instructions [here](./docs/hello_world.md).

Absolute hello world instructions [here](https://github.com/kathy-t/hello_world/blob/master/docs/hello_world.md).

Relative goodbye world instructions [here](./goodbye_world.md).
